# Projeto de Classificação de Anúncios

## Propósito

O propósito deste projeto é desenvolver um modelo de classificação que possa prever se um usuário irá clicar em um anúncio online ou não, com base em dados demográficos e comportamentais.

## Problema de Negócio

Estamos resolvendo um problema de negócio relevante para o marketing digital, que é a otimização de campanhas publicitárias online. Ao prever a probabilidade de um usuário clicar em um anúncio, podemos segmentar melhor o público-alvo.

## Estrutura do Repositório

O repositório está organizado em pastas referentes a cada etapa da elaboração de um modelo de classificação. As pastas são numeradas de acordo com o fluxo do projeto.

-   00_dados: Inclui os dados brutos para o projeto, no formato CSV.
-   01_pre_process: Inclui o script de limpeza da base de dados, o arquivo `advertising_clean.csv` com os dados limpos e um script utilizado para análise dos tópicos de anúncio com a API VertexIA.
-   02_EDA: Contém um notebook para a exploração estatística e descritiva de cada variável, com visualização, correlação e distribuição das variáveis.
-   03_teste_estatístico: Contém o notebook de teste estatístico dos dados, como teste de hipóteses, para normalidade e teste não paramétrico de Mann-Whitney.
-   04_modelo_entrada: Contém os notebooks de modelagem dos dados, como seleção de variáveis, divisão de treino e teste, e otimização de hiperparâmetros do modelo selecionado.
-   05_modelo_saida: Contém os notebooks de avaliação do modelo escolhido, como métricas de desempenho, matriz de confusão.

## Como Executar o Projeto

Para executar o projeto, é necessário ter o Python instalado na sua máquina, bem como as bibliotecas listadas no arquivo `requirements.txt`. Faça a instalação com `pip install -r requirements.txt`. Recomendamos que utilize um ambiente virtual.

Os scripts são independentes, as pastas são sequenciadas de acordo com o fluxo do projeto. Você pode executar cada script ou notebook individualmente, ou seguir a ordem das pastas.

Os arquivos CSV já estão prontos para serem utilizados, não é necessário rodar o pré-processamento novamente.

Os notebooks `.ipynb` possuem Markdown comentando sobre o código, os resultados e as conclusões de cada etapa.

## Contribuições

Se você deseja contribuir, crie uma nova branch e faça uma solicitação de merge para a branch `dev`.

Os commits são iniciados por uma abreviação do que aquela alteração representa:

-   [ADD] é utilizado para adições
-   [CHG] é utilizado para alterações
-   [DEL] é utilizado para deleções

Solicitações de merge para a branch `main` não serão aceitas.

## FAQ

-   Preciso rodar o pré-processamento? Não, os dados já estão limpos e prontos para serem utilizados.
-   E as bibliotecas? Você precisa instalar as bibliotecas listadas no arquivo `requirements.txt`. Você pode usar o comando `pip install -r requirements.txt`.
-   Como faço para executar o projeto? Os scripts são independentes, as pastas são sequenciadas de acordo com o fluxo do projeto. Você pode executar cada script ou notebook individualmente, ou seguir a ordem das pastas.
-   Como faço para entrar em contato? Você pode entrar em contato com o autores do projeto por e-mail ou pelo GitLab.

## Autores

Gisele Beltramini  
João Gabriel Danelon  
João Victor Cadiolli  
Naiara da Gama  
Paula Santos
