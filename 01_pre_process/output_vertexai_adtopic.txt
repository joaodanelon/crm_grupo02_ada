############################################# Prompt 0 ############################################# 
  1. Customer-centric 

- Customer-focused solution-oriented software
- Customer-focused explicit challenge
- Customer-focused fault-tolerant implementation
- Customer-focused multi-tasking Internet solution
- Customer-focused 24/7 concept


2. Process-centric 

- Versatile content-based protocol
- Advanced 24/7 productivity
- Automated directional function
- Balanced uniform algorithm
- Operative multi-tasking Graphic Interface


3. Technology-centric 

- Fully-configurable context-sensitive Graphic Interface
- Total user-facing hierarchy
- Profound stable product
- Synergized context-sensitive database
- Function-based optimizing protocol



############################################# Prompt 1 ############################################# 
  1. Productivity 

- Advanced 24/7 productivity
-  Automated directional function
-  Advanced systemic productivity


2. Technology 

- Versatile content-based protocol
-  Balanced uniform algorithm
-  Operative multi-tasking Graphic Interface


3. Business

- Total user-facing hierarchy
-  Profound stable product
-  Synergized context-sensitive database



############################################# Prompt 2 ############################################# 
  **1. Business & Finance** 
 - Versatile content-based protocol
 - Horizontal global leverage
 - Synergized context-sensitive database


**2. Technology** 
 - Advanced 24/7 productivity
 - Automated directional function
 - Balanced uniform algorithm


**3. Healthcare** 
 - Operative multi-tasking Graphic Interface
 - Fully-configurable context-sensitive Graphic Interface
 - Total user-facing hierarchy



############################################# Prompt 3 ############################################# 
  1.  Versatile content-based protocol
-  Advanced 24/7 productivity
-  Automated directional function
-  Balanced uniform algorithm
-  Operative multi-tasking Graphic Interface


2.  Fully-configurable context-sensitive Graphic Interface
-  Total user-facing hierarchy
-  Profound stable product
-  Synergized context-sensitive database
-  Function-based optimizing protocol
-  Horizontal global leverage


3.  Configurable dynamic adapter
-  Intuitive global website
-  Advanced systemic productivity
-  Fully-configurable client-driven customer loyalty
-  Self-enabling incremental collaboration




